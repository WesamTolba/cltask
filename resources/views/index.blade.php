@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-top:2rem;"> 
                    
                <div class="card-header">List  of appointments</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Patient naem</th>
                                <th scope="col">Doctor name</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($appointments as $appointment)
                                    <tr>
                                        <th scope="row">{{$count++}}</th>
                                        <td>{{$appointment->patientName->username}}</td>
                                        <td>{{$appointment->doctorName->username}}</td>
                                        <td>{{Carbon\Carbon::parse($appointment->date)->format('d-M-Y')}}</td>
                                        <td>{{Carbon\Carbon::parse($appointment->time)->format('h:i')}}</td>
                                        @if ($appointment->confirmed == 0)
                                            <td>Pending</td>
                                            <td><a href="{{route('confirmed',$appointment->id)}}" class="btn btn-sm btn-info">Confirmed</a></td>
                                        @else
                                            <td>Confirmed</td> 
                                            <td><a  href="{{route('confirmed',$appointment->id)}}" class="btn btn-sm btn-warning">Reject</a></td>  
                                        @endif
                                        
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
