<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\PainList;
use App\UserRole;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count        = 1;
        $id           = auth()->user()->id;
        $appointments = Appointment::where('patient_id',$id)->orWhere('doctor_id',$id)->get();

        return view('index',compact('count','appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $painList = PainList::get(['id','name']);
        $roles = Role::where('name','!=','Admin')->get(['id','name']);
        return view('personalInformation ',compact('painList','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = auth()->user();
        
        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $request->role_id,
            ]);

        $user->update([

                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
               // 'email'      => $request->email,
                'mobile'     => $request->mobile,
                'birth_date' => $request->birth_date,
                'gender'     => $request->gender,
                'country'    => $request->country,
                'occupation' => $request->occupation,
                'pain_id'    => $request->pain_id,
        ]);
        return redirect()->route('userHome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirmed($id)
    {
        $appointment = Appointment::findOrfail($id);

        
        $appointment->update([
            'confirmed' =>  !$appointment->confirmed
        ]);
        if($appointment->confirmed == 1){
            return redirect()->route('userHome')->with('info','User was approved successfully!');
        }else {
            return redirect()->route('userHome')->with('warning','User was rejected successfully!');
        }
    }

    
}
