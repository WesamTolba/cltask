<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            //'email'      => 'required|email|unique:users,email,'.auth()->user()->id,
            'mobile'     => 'required|unique:users,mobile,'.auth()->user()->id,
            'birth_date' => 'required',
            'gender'     => 'required',
            'country'    => 'required',
            'occupation' => 'required',
        ];
    }
}
