<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['auth','isNotAdmin']],function(){
    
    Route::get('/userHome', 'UserController@index')->name('userHome');
    Route::get('/personalInformation', 'UserController@create')->name('personalInformation');
    Route::post('/saveInformation', 'UserController@store')->name('saveInformation');
    Route::get('/confirmed/{id}','UserController@confirmed')->name('confirmed');
});

Auth::routes();


Route::group(['middleware' => ['auth','isAdmin']],function(){
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::post('/assignAppointment', 'AdminController@assignAppointment')->name('assignAppointment');
});