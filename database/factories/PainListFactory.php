<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PainList;
use Faker\Generator as Faker;

$factory->define(PainList::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'desc' => $faker->paragraph,
    ];
});
